/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem;

import com.pb.ordersystem.jpa.domain.Product;
import com.pb.ordersystem.jpa.domain.CustomerOrder;
import com.pb.ordersystem.jpa.service.CustomerOrderRepository;
import com.pb.ordersystem.jpa.service.ProductRepository;
import com.pb.ordersystem.web.Endpoint;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import javax.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author abeggs
 */
@Log4j
public abstract class AbstractTest {

    protected static final String CUSTOMER_ORDER_ENDPOINT = "/customerorder";
    protected static final String PRODUCT_ENDPOINT = "/" + Endpoint.PRODUCT;
    protected static final MediaType JSON_CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext context;
    protected MockMvc mvc;
    HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    protected ProductRepository productRepository;
    @Autowired
    protected CustomerOrderRepository customerOrderRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
        customerOrderRepository.deleteAll();
        assertThat(customerOrderRepository.count(), equalTo(0L));
        productRepository.deleteAll();
        assertThat(productRepository.count(), equalTo(0L));
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = java.util.Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();
        assertThat(this.mappingJackson2HttpMessageConverter, notNullValue());
    }

    protected static Matcher<Throwable> isNestedConstraintViolationException() {
        return new BaseMatcher<Throwable>() {
            @Override
            public boolean matches(Object object) {
                boolean matches = false;
                if (object instanceof Throwable) {
                    Throwable parent = (Throwable) object;
                    Throwable cause = parent.getCause();
                    matches = cause instanceof ConstraintViolationException;
                }
                return matches;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    protected String toJson(Object object) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                object, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        String json = mockHttpOutputMessage.getBodyAsString();
        if (log.isDebugEnabled()) {
            log.debug("converting " + object + " to json " + json);
        }
        return json;
    }

    protected Product createValidProduct() {
        Product product = new Product();
        product.setName("jelly");
        product.setDescription("This is especially great with peanut butter");
        product.setItemsInStock(1);
        product.setPrice(1.00);
        return product;
    }

    protected CustomerOrder createCustomerOrder(String customerName, List<Product> products) {
        CustomerOrder userOrder = new CustomerOrder();
        userOrder.setCustomerName(customerName);
        userOrder.setBillingAddress("Port Jefferson, NY");
        userOrder.setShippingAddress("Danbury, CT");
        if (products != null) {
            userOrder.setProducts(products);
        }
        return userOrder;
    }

    protected String addCustomerOrder(CustomerOrder customerOrder) throws Exception {
        String payload = toJson(customerOrder);
        return mvc.perform(post(CUSTOMER_ORDER_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getHeader("Location");
    }

    protected String addProduct(Product product) throws Exception {
        String payload = toJson(product);
        return mvc.perform(post(PRODUCT_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getHeader("Location");
    }

    protected void addProducts(Product template, int count) throws Exception, IOException {
        String payload = toJson(template);
        for (int i = 0; i < count; i++) {
            mvc.perform(post(PRODUCT_ENDPOINT)
                    .content(payload)
                    .contentType(JSON_CONTENT_TYPE))
                    .andExpect(status().isCreated());
        }
    }

}
