/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.AbstractTest;
import com.pb.ordersystem.OrderSystemApplication;
import com.pb.ordersystem.jpa.domain.Product;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 *
 * @author abeggs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderSystemApplication.class)
@WebAppConfiguration
// Separate profile for web tests to avoid clashing databases
@ActiveProfiles("test")
@Log4j
public class ProductResourceTest extends AbstractTest {

    private static final String PRODUCT_FIND_BY_NAME_ENDPOINT = "/product/search/findByNameContaining?name=%s";

    @Test
    public void testAddInvalidItemsInStockProduct() throws Exception {
        thrown.expect(isNestedConstraintViolationException());
        Product product = new Product();
        product.setName("jelly");
        product.setDescription("This is especially great with peanut butter");
        // price can be free
        product.setPrice(0);
        // set an invalid value for items in stock, we should not have anything
        // less than 0
        // this should cause a 500 error
        product.setItemsInStock(-1);

        String payload = toJson(product);
        mvc.perform(post(PRODUCT_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE));
    }

    @Test
    public void testAddInvalidPriceProduct() throws Exception {
        thrown.expect(isNestedConstraintViolationException());
        Product product = new Product();
        product.setName("jelly");
        product.setDescription("This is especially great with peanut butter");
        product.setItemsInStock(1);
        // set an invalid value for price, we should not have anything
        // less than 0
        // this should cause a 500 error
        product.setPrice(-1);

        String payload = toJson(product);
        mvc.perform(post(PRODUCT_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE));
    }

    @Test
    public void testAddInvalidDescriptionProduct() throws Exception {
        thrown.expect(isNestedConstraintViolationException());
        Product product = new Product();
        product.setName("jelly");
        product.setItemsInStock(1);
        product.setPrice(1.00);
        // set an invalid value for description
        // this should cause a 500 error
        product.setDescription(null);

        String payload = toJson(product);
        mvc.perform(post(PRODUCT_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE));
    }

    @Test
    public void testAddInvalidNameProduct() throws Exception {
        thrown.expect(isNestedConstraintViolationException());
        Product product = new Product();
        product.setDescription("This is especially great with peanut butter");
        product.setItemsInStock(1);
        product.setPrice(1.00);
        // set an invalid value for description
        // this should cause a 500 error
        product.setName(null);

        String payload = toJson(product);
        mvc.perform(post(PRODUCT_ENDPOINT)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE));
    }

    @Test
    public void testAddProduct() throws Exception {
        Product template = createValidProduct();
        addProducts(template, 5);
        assertThat(productRepository.count(), is(5L));

        for (Product savedProduct : productRepository.findAll()) {
            log.debug(savedProduct);
            assertThat(savedProduct, equalTo(template));
        }
    }

    @Test
    public void testSearchByNameContainingWithNoProducts() throws Exception {
        assertThat(productRepository.count(), is(0L));
        String path = String.format(PRODUCT_FIND_BY_NAME_ENDPOINT, "nomatchvalue");
        mvc.perform(get(path)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                // probably a better way to ensure that the content is empty
                // using jsonPath
                .andExpect(content().string("{ }"));
    }

    @Test
    public void testSearchByNameContainingWithProducts() throws Exception {
        Product template = createValidProduct();
        addProducts(template, 5);
        assertThat(productRepository.count(), is(5L));
        String path = String.format(PRODUCT_FIND_BY_NAME_ENDPOINT, "jelly");
        log.info("path: " + path);
        mvc.perform(get(path)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$_embedded.product", hasSize(5)));
    }

    @Test
    public void testGetProducts() throws Exception {
        Product template = createValidProduct();
        addProducts(template, 5);
        assertThat(productRepository.count(), is(5L));
        mvc.perform(get(PRODUCT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$_embedded.product", hasSize(5)));
    }

    @Test
    public void testDeleteProductWithoutId() throws Exception {
        mvc.perform(delete(PRODUCT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testDeleteProductWithNonexistentId() throws Exception {
        mvc.perform(delete(PRODUCT_ENDPOINT + "/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteProductWithValidId() throws Exception {
        Product product = createValidProduct();
        product = productRepository.save(product);
        String path = PRODUCT_ENDPOINT + "/" + product.getId();
        mvc.perform(delete(path)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testUpdateProductWithInvalidId() throws Exception {
        Product product = createValidProduct();
        String payload = toJson(product);
        log.debug("payload: " + payload);
        long id = 1L;
        String path = PRODUCT_ENDPOINT + "/" + id;
        log.info("put call on " + path + " with payload: " + payload);
        ResultActions resultActions = mvc.perform(put(path)
                .accept(MediaType.APPLICATION_JSON)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE))
                .andExpect(status().isCreated());
        String location = resultActions.andReturn().getResponse().getHeader("Location");
        String indexString = location.substring(location.lastIndexOf("/") + 1);
        // expect that we have created a new product even if it didn't exist
        assertThat(productRepository.count(), is(1L));
        Product savedProduct = productRepository.findOne(Long.parseLong(indexString));
        assertThat(savedProduct, equalTo(product));
    }

    @Test
    public void testUpdateProductWithValidId() throws Exception {
        // don't want to assume we are correctly setting the id
        // add an additional product to differentiate between the two
        // saved product entities
        Product secondProduct = createValidProduct();
        secondProduct.setName("hot... pockets");
        secondProduct.setDescription("scald your tongue while you eat");
        secondProduct.setPrice(5.00);
        secondProduct.setItemsInStock(2);
        productRepository.save(secondProduct);

        Product product = createValidProduct();
        productRepository.save(product);

        Product productUpdate = createValidProduct();
        productUpdate.setName("fluff");
        productUpdate.setItemsInStock(5);
        String payload = toJson(productUpdate);

        String path = PRODUCT_ENDPOINT + "/" + product.getId();
        mvc.perform(put(path)
                .accept(MediaType.APPLICATION_JSON)
                .content(payload)
                .contentType(JSON_CONTENT_TYPE))
                .andExpect(status().isNoContent());

        Product savedProduct = productRepository.findOne(product.getId());

        assertThat(savedProduct, equalTo(productUpdate));
        assertThat(productRepository.count(), is(2L));
    }

}
