/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.AbstractTest;
import com.pb.ordersystem.OrderSystemApplication;
import com.pb.ordersystem.jpa.domain.Product;
import lombok.extern.log4j.Log4j;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author abeggs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderSystemApplication.class)
@WebAppConfiguration
// Separate profile for web tests to avoid clashing databases
@ActiveProfiles("test")
@Log4j
public class ProductServiceTest extends AbstractTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testOrder() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(1);
        productService.order(product);
        assertThat(product.getItemsInStock(), is(0L));
    }

    @Test
    public void testOrderWithInsufficientItemsInStock() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(1);
        productService.order(product);
        try {
            productService.order(product);
        } catch (OutOfStockException ex) {
            assertThat(product.getItemsInStock(), is(0L));
        }
    }

    @Test
    public void testReorder() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(1);
        productService.reorder(product);
        assertThat(product.getItemsInStock(), is(6L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReorderWithIncorrectReorderCount() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(1);
        productService.reorder(product, -1);
    }
}
