/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.AbstractTest;
import com.pb.ordersystem.OrderSystemApplication;
import com.pb.ordersystem.jpa.domain.CustomerOrder;
import com.pb.ordersystem.jpa.domain.Product;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abeggs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderSystemApplication.class)
@WebAppConfiguration
// Separate profile for web tests to avoid clashing databases
@ActiveProfiles("test")
@Log4j
public class CustomerOrderResourceTest extends AbstractTest {

    @Test
    @Transactional
    public void testAddCustomerOrderWithProductsCustomerOrder() throws Exception {
        List<Product> products = new ArrayList<>();
        Product product = createValidProduct();
        product = productRepository.save(product);
        products.add(product);
        CustomerOrder customerOrder = createCustomerOrder("abeggs", products);
        customerOrder = customerOrderRepository.save(customerOrder);

        CustomerOrder foundOrder = customerOrderRepository.findOne(customerOrder.getNumber());
        log.info("foundOrder: " + foundOrder);

        String payload = toJson(foundOrder);
        log.info("addCustomerOrder payload: " + payload);
    }

    @Test
    public void testGetCustomerOrderWithProductsCustomerOrder() throws Exception {
        List<Product> products = new ArrayList<>();
        Product product = createValidProduct();
        product = productRepository.save(product);
        products.add(product);
        CustomerOrder customerOrder = createCustomerOrder("abeggs", products);
        customerOrderRepository.save(customerOrder);

        String content = mvc.perform(get(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("content:  " + content);
    }

    @Test
    public void testUnableToDeleteCustomerOrder() throws Exception {
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);

        // Test that we cannot call the delete method
        mvc.perform(delete(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testUpdateShippingAddressCustomerOrder() throws Exception {
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);

        CustomerOrder updatedCustomerOrder = createCustomerOrder("abeggs", null);
        updatedCustomerOrder.setShippingAddress("Bethlehem, PA");
        mvc.perform(patch(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber())
                .content(toJson(updatedCustomerOrder))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        CustomerOrder savedUpdatedCustomerOrder = customerOrderRepository.findOne(customerOrder.getId());
        assertThat(updatedCustomerOrder.getShippingAddress(), is(equalTo(savedUpdatedCustomerOrder.getShippingAddress())));
    }

    @Test
    public void testUpdateBillingAddressCustomerOrder() throws Exception {
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);

        CustomerOrder updatedCustomerOrder = createCustomerOrder("abeggs", null);
        updatedCustomerOrder.setBillingAddress("Bethlehem, PA");
        mvc.perform(patch(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber())
                .content(toJson(updatedCustomerOrder))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        CustomerOrder savedUpdatedCustomerOrder = customerOrderRepository.findOne(customerOrder.getId());
        assertThat(updatedCustomerOrder.getBillingAddress(), is(equalTo(savedUpdatedCustomerOrder.getBillingAddress())));
    }

    @Test
    public void testSearchForPastOrdersCustomerOrderByNameWithNoMatch() throws Exception {
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);
        assertThat(customerOrderRepository.count(), is(1L));
        String path = String.format(CUSTOMER_ORDER_ENDPOINT + "/search/findByCustomerNameAllIgnoringCase?%s", "jelly");
        log.info("path: " + path);
        mvc.perform(get(path)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{ }"));
    }

    @Test
    public void testSearchForPastOrdersCustomerOrderByName() throws Exception {
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);
        assertThat(customerOrderRepository.count(), is(1L));
        String path = String.format(CUSTOMER_ORDER_ENDPOINT + "/search/findByCustomerNameAllIgnoringCase?name=%s", "abeggs");
        log.info("path: " + path);
        mvc.perform(get(path)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$_embedded.customerorder", hasSize(1)));
    }

}
