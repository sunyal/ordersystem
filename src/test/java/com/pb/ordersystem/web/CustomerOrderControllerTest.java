/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.web;

import com.pb.ordersystem.AbstractTest;
import com.pb.ordersystem.OrderSystemApplication;
import com.pb.ordersystem.jpa.domain.CustomerOrder;
import com.pb.ordersystem.jpa.domain.Product;
import com.pb.ordersystem.jpa.service.OutOfStockException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.NestedServletException;

/**
 *
 * @author abeggs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderSystemApplication.class)
@WebAppConfiguration
// Separate profile for web tests to avoid clashing databases
@ActiveProfiles("test")
@Log4j
public class CustomerOrderControllerTest extends AbstractTest {

    private static final String TEXT_URI_LIST_MEDIA_TYPE = "text/uri-list";

    @Test
    @Transactional
    public void testAddProductToCustomerOrder() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(1);
        productRepository.save(product);
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);

        mvc.perform(post(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber() + "/products/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        assertThat(productRepository.findOne(product.getId()).getItemsInStock(), is(0L));
    }

    @Test
    @Transactional
    public void testAddProductToCustomerOrderWithInsufficientStock() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(0);
        productRepository.save(product);
        CustomerOrder customerOrder = createCustomerOrder("abeggs", null);
        customerOrderRepository.save(customerOrder);

        try {
            mvc.perform(post(CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber() + "/products/" + product.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent());
            fail("Should have thrown a nested Exception with OutOfStockException");
        } catch (NestedServletException ex) {
            assertThat(ex.getCause(), is(instanceOf(OutOfStockException.class)));
        }
        // ensure that it was reordered after the failure
        assertThat(productRepository.findOne(product.getId()).getItemsInStock(), is(5L));
    }

    @Test
    @Transactional
    public void testGetCustomerOrderProducts() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(0);
        productRepository.save(product);
        List<Product> products = new ArrayList<>();
        products.add(product);
        CustomerOrder customerOrder = createCustomerOrder("abeggs", products);
        customerOrderRepository.save(customerOrder);
        final String path = CUSTOMER_ORDER_ENDPOINT + "/" + customerOrder.getNumber() + "/products";

        mvc.perform(get(path)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                // since we aren't using the Repository calls with HATEOAS
                // we don't have the $._embedded.product path
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
    }
}
