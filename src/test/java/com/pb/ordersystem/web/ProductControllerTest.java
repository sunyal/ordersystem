/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.web;

import com.pb.ordersystem.AbstractTest;
import com.pb.ordersystem.OrderSystemApplication;
import com.pb.ordersystem.jpa.domain.Product;
import lombok.extern.log4j.Log4j;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author abeggs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderSystemApplication.class)
@WebAppConfiguration
@ActiveProfiles("test")
@Log4j
public class ProductControllerTest extends AbstractTest {

    @Test
    public void testOrderWithInsufficientStock() throws Exception {
        Product product = createValidProduct();
        product.setItemsInStock(10L);
        productRepository.save(product);
        final String path = PRODUCT_ENDPOINT + "/" + product.getId() + "/reorder";
        log.info("test path: " + path);
        mvc.perform(post(path)
                .contentType(JSON_CONTENT_TYPE))
                .andExpect(status().isNoContent());
        assertThat(productRepository.findOne(product.getId()).getItemsInStock(), is(15L));
    }

}
