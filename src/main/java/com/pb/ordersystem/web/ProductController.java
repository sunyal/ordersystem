/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.web;

import com.pb.ordersystem.jpa.domain.Product;
import com.pb.ordersystem.jpa.service.CustomerOrderRepository;
import com.pb.ordersystem.jpa.service.ProductRepository;
import com.pb.ordersystem.jpa.service.ProductService;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes additional REST calls for the {@link Product}s above and beyond the
 * CRUD supplied by {@link ProductRepository}
 *
 * @author abeggs
 */
@Log4j
@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;

    /**
     * Exposed endpoint that reorders the {@link Product}.
     *
     * @param productId the id of the product.
     * @param response used to set the status code of the response.
     */
    @Transactional
    @RequestMapping(value = "/" + Endpoint.PRODUCT + "/{productId}/reorder", method = POST)
    public void reorderProduct(@PathVariable long productId, HttpServletResponse response) {
        Product product = productRepository.findOne(productId);
        if (product == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            productService.reorder(product);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }

}
