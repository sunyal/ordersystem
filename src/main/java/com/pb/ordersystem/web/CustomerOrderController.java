/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.web;

import com.pb.ordersystem.jpa.domain.CustomerOrder;
import com.pb.ordersystem.jpa.domain.Product;
import com.pb.ordersystem.jpa.service.CustomerOrderRepository;
import com.pb.ordersystem.jpa.service.OutOfStockException;
import com.pb.ordersystem.jpa.service.ProductRepository;
import com.pb.ordersystem.jpa.service.ProductService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller for the {@code /customerorder/{id}/products} path was needed
 * to supplement the {@link ProductRepository} REST API. It doesn't seem like a
 * simple task to override the save method in the Repository and provide custom
 * behavior. This seemed the best approach with my limited knowledge of Spring
 * Boot and Data.
 *
 * @author abeggs
 */
@Log4j
@RestController
public class CustomerOrderController {

    @Autowired
    private CustomerOrderRepository customerOrderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;

    /**
     * This is implemented using the customer id and the product id and a POST.
     * Attempted to use the traditional <code>text/uri-list</code> media type
     * and PATCH, however was unsuccessful in implementation. The documentation
     * is very light in this area, if I had more time I would add it this way as
     * this seems to be the more 'correct' way.
     *
     * @param customerOrderId The id of the {@link CustomerOrder} to look up and
     * add a product to the order.
     * @param productId the id of the {@link Product} to add to the
     * CustomerOrder.
     * @param response the response that can be modified to update the response
     * status code.
     * @throws OutOfStockException if the product is ordered and we don't have a
     * sufficient amount of stock to satisfy the order then this will be thrown.
     */
    @Transactional
    @RequestMapping(value = Endpoint.CUSTOMER_ORDER + "/{id}/products/{productId}", method = POST)
    public void associateProductToCustomerOrder(@PathVariable("id") long customerOrderId,
            @PathVariable("productId") long productId,
            HttpServletResponse response) throws OutOfStockException {
        Product persistedProduct = productRepository.findOne(productId);
        log.debug("product: " + persistedProduct);
        CustomerOrder customerOrder = customerOrderRepository.findOne(customerOrderId);
        if (customerOrder == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            log.info("add product " + persistedProduct);
            try {
                productService.order(persistedProduct);
            } catch (OutOfStockException ex) {
                productService.reorder(persistedProduct);
                throw ex;
            }
            customerOrder.getProducts().add(persistedProduct);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }

    /**
     * Returns a list of {@link Product}s associated to a given CustomerOrder
     * id.
     *
     * @param customerOrderId the id of the CustomerOrder
     * @param response the response that can be modified to update the response
     * status code.
     * @return a list of {@link Product}s associated to a given CustomerOrder
     * id.
     *
     */
    @Transactional(readOnly = true)
    @RequestMapping(value = Endpoint.CUSTOMER_ORDER + "/{customerOrderId}/products", method = GET)
    public List<Product> getCustomerOrderProducts(@PathVariable long customerOrderId, HttpServletResponse response) {
        List<Product> products = new ArrayList<>();
        CustomerOrder customerOrder = customerOrderRepository.findOne(customerOrderId);
        if (customerOrder == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            if (customerOrder.getProducts() != null) {
                products = customerOrder.getProducts();
            }
            response.setStatus(HttpServletResponse.SC_OK);
        }
        return products;
    }

}
