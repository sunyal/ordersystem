/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.jpa.domain.Product;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides ordering and reordering business logic for the products.
 *
 * @author abeggs
 */
@Service
@Log4j
public class ProductService {

    private static final int DEFAULT_REORDER_COUNT = 5;

    @Autowired
    private ProductRepository productRepository;

    /**
     * Submit an order for the given {@link Product}. This will decrement the
     * itemsInStock count by 1 and persist this to the database. If we do not
     * have enough stock to satisfy the order then an
     * {@link OutOfStockException} will be thrown.
     *
     * @param product the product that will attempt to be ordered, cannot be
     * null.
     * @throws OutOfStockException If we do not have enough stock to satisfy the
     * order then an {@link OutOfStockException} will be thrown.
     * @throws IllegalArgumentException thrown if {@link Product} is null.
     */
    @Transactional
    public void order(final Product product) throws OutOfStockException, IllegalArgumentException {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        log.info("Order product " + product);
        long count = product.getItemsInStock();
        log.info("Product ItemsInStock value " + count);
        if (count <= 0) {
            throw new OutOfStockException("There are no more items in stock for the product");
        } else {
            long newStockValue = count - 1;
            log.info("Updating the items in stock to the new count " + newStockValue);
            product.setItemsInStock(newStockValue);
            productRepository.save(product);
        }
    }

    /**
     * This will restock the {@link Product} with a reorder. This will be
     * restocked by the passed in count and persisted.
     *
     * @param product the product that will be attempted to order, cannot be
     * null.
     * @param count the number of items to order and restock the product. Must
     * be greater than 0.
     * @throws IllegalArgumentException thrown if the product is null or the
     * count is 0 or less.
     */
    @Transactional
    public void reorder(final Product product, int count) throws IllegalArgumentException {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        if (count <= 0) {
            throw new IllegalArgumentException("The count was " + count + " and cannot be 0 or less");
        }
        long newStockValue = product.getItemsInStock() + count;
        log.info("Updating the items in stock to the new count " + newStockValue);
        product.setItemsInStock(newStockValue);
        productRepository.save(product);
    }

    /**
     * This will restock the {@link Product} with a reorder. This will be
     * restocked by the default count {@link DEFAULT_REORDER_COUNT} and
     * persisted.
     *
     * @param product the product that will attempt to order, cannot be null.
     * @throws IllegalArgumentException thrown if the product is null.
     */
    @Transactional
    public void reorder(final Product product) throws IllegalArgumentException {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        reorder(product, DEFAULT_REORDER_COUNT);
    }

}
