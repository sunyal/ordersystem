/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.jpa.domain.Product;
import com.pb.ordersystem.web.Endpoint;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

/**
 * Exposes the {@link Product} resource using the Spring Data Repository
 * endpoint pattern
 *
 * @author abeggs
 */
@Transactional
@RepositoryRestResource(collectionResourceRel = Endpoint.PRODUCT, path = Endpoint.PRODUCT)
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    List<Product> findByNameContaining(@Param("name") String name);

    List<Product> findByDescriptionContaining(@Param("description") String description);

    @Query("select p from #{#entityName} p where p.name LIKE %:searchValue% OR p.description LIKE %:searchValue%")
    List<Product> findByNameOrDescriptionContaining(@Param("searchValue") String value);
}
