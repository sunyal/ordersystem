/*
 * Copyright 2015 Alex Beggs.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pb.ordersystem.jpa.service;

import com.pb.ordersystem.jpa.domain.CustomerOrder;
import com.pb.ordersystem.web.Endpoint;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Exposes the {@link CustomerOrder} resource using the Spring Data Repository
 * endpoint pattern
 *
 * @author abeggs
 */
@RepositoryRestResource(collectionResourceRel = Endpoint.CUSTOMER_ORDER, path = Endpoint.CUSTOMER_ORDER)
public interface CustomerOrderRepository extends PagingAndSortingRepository<CustomerOrder, Long> {

    /**
     * Search for the CustomerOrders using the customerName property.
     *
     * @param name the name of the customer.
     * @return the list of matching {@link CustomerOrder}s
     */
    List<CustomerOrder> findByCustomerNameAllIgnoringCase(@Param("name") String name);

    ////////////////////////////////////////////////////////////
    // Disable the DELETE methods, as this is not a requirement
    ////////////////////////////////////////////////////////////
    @Override
    @RestResource(exported = false)
    void delete(CustomerOrder entity);

    @Override
    @RestResource(exported = false)
    void delete(Long id);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void delete(Iterable<? extends CustomerOrder> entities);

}
