[![Build Status](https://drone.io/bitbucket.org/sunyal/ordersystem/status.png)](https://drone.io/bitbucket.org/sunyal/ordersystem/latest)
# README #

This is the Pitney Bowes Order System assignment.  The task was to create a system that manages products, inventories and orders.  The system exposes a REST API.

### How do I get set up? ###

* Postman

    Postman json file that provides calls to represent the user stories.     https://www.getpostman.com/collections/b1c99de589c63757ed53

    This can be imported into Postman (Chrome plugin) and executed against localhost:8080

* Database configuration
   
Alternative Docker MySQL setup
      - Requires modification (assuming not running on Linux Host) of the application.properties to point to the boot2docker VM.

    boot2docker ip

      192.168.59.103

   application.properties change spring.datasource.url=mysql://192.168.59.103/ordersystem

   Run docker mysql

    docker run -e MYSQL_DATABASE=ordersystem -e MYSQL_USER=dbuser -e MYSQL_PASSWORD=dbpass -e MYSQL_ROOT_PASSWORD=admin -p 3306:3306 --name mysql -it  mysql
  
   MySQL (src/main/resources/application.properties)

    spring.datasource.url=jdbc:mysql://localhost/ordersystem
    spring.datasource.username=dbuser
    spring.datasource.password=dbpass
    
  Test database uses in memory hsqldb

### Who do I talk to? ###

* Alex Beggs <sunyal@gmail.com>